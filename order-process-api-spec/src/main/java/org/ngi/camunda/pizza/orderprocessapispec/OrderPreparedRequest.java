package org.ngi.camunda.pizza.orderprocessapispec;

import java.util.Map;

public class OrderPreparedRequest {

    private String orderProcessInstanceId;
    private String message;

    public String getOrderProcessInstanceId() {
        return orderProcessInstanceId;
    }

    public void setOrderProcessInstanceId(String orderProcessInstanceId) {
        this.orderProcessInstanceId = orderProcessInstanceId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
