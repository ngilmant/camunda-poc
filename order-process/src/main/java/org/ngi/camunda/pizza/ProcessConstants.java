package org.ngi.camunda.pizza;

public class ProcessConstants {

  public static final String PROCESS_DEFINITION_KEY = "order-process"; // BPMN Process ID

  public static final String PREPARE_ORDER_PROCESS_DEFINITION_KEY = "prepare-order-process";
  //Variables.
  public static final String ORDER_PROCESS_INSTANCE_ID = "orderProcessInstanceId";
  public static final String CLIENT_NAME = "clientName";
  public static final String CLIENT_ORDER_DETAIL = "orderDetail";

  //Prepare ORDER Instance ID
  public static final String PREPARE_ORDER_PROCESS_INSTANCE_ID = "prepareOrderProcessInstanceId";

}
