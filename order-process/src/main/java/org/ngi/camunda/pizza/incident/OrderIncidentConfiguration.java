package org.ngi.camunda.pizza.incident;

import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.impl.cfg.AbstractProcessEnginePlugin;
import org.camunda.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.camunda.bpm.engine.impl.cfg.ProcessEnginePlugin;
import org.camunda.bpm.engine.runtime.Incident;
import org.camunda.bpm.spring.boot.starter.configuration.Ordering;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
@Order(Ordering.DEFAULT_ORDER + 1)
public class OrderIncidentConfiguration extends AbstractProcessEnginePlugin {

    @Override
    public void preInit(ProcessEngineConfigurationImpl processEngineConfiguration) {
        ArrayList customIncidentHandlers = new ArrayList();
        customIncidentHandlers.add(new OrderIncidentHandler(Incident.FAILED_JOB_HANDLER_TYPE));
        customIncidentHandlers.add(new OrderIncidentHandler(Incident.EXTERNAL_TASK_HANDLER_TYPE));
        processEngineConfiguration.setCustomIncidentHandlers(customIncidentHandlers);
    }

    @Override
    public void postInit(ProcessEngineConfigurationImpl processEngineConfiguration) {

    }

    @Override
    public void postProcessEngineBuild(ProcessEngine processEngine) {

    }
}
