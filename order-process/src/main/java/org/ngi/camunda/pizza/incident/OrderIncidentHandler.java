package org.ngi.camunda.pizza.incident;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.impl.context.Context;
import org.camunda.bpm.engine.impl.incident.DefaultIncidentHandler;
import org.camunda.bpm.engine.impl.incident.IncidentContext;
import org.camunda.bpm.engine.impl.incident.IncidentHandler;
import org.camunda.bpm.engine.impl.persistence.entity.IncidentEntity;
import org.camunda.bpm.engine.runtime.Incident;

import java.util.List;

@Slf4j
public class OrderIncidentHandler implements IncidentHandler {

    protected String type;

    public OrderIncidentHandler(String type) {
        this.type = type;
    }

    public String getIncidentHandlerType() {
        return type;
    }

    @Override
    public Incident handleIncident(IncidentContext context, String message) {
        Incident incident = createIncident(context, message);
        //add here custom Code..
        log.error("OrderIncidentHandler CALLED with message {} for Incident {}", message, incident);

        return incident;
    }

    @Override
    public void resolveIncident(IncidentContext context) {
        removeIncident(context, true);
    }

    @Override
    public void deleteIncident(IncidentContext context) {
        removeIncident(context, false);
    }

    public Incident createIncident(IncidentContext context, String message) {
        IncidentEntity newIncident = IncidentEntity.createAndInsertIncident(type, context, message);

        if(context.getExecutionId() != null) {
            newIncident.createRecursiveIncidents();
        }

        return newIncident;
    }

    protected void removeIncident(IncidentContext context, boolean incidentResolved) {
        List<Incident> incidents = Context
                .getCommandContext()
                .getIncidentManager()
                .findIncidentByConfiguration(context.getConfiguration());

        for (Incident currentIncident : incidents) {
            IncidentEntity incident = (IncidentEntity) currentIncident;
            if (incidentResolved) {
                incident.resolve();
            } else {
                incident.delete();
            }
        }
    }
}
