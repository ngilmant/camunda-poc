package org.ngi.camunda.pizza.process;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.rest.dto.VariableValueDto;
import org.camunda.bpm.engine.rest.dto.runtime.ProcessInstanceDto;
import org.camunda.bpm.engine.rest.dto.runtime.StartProcessInstanceDto;
import org.ngi.camunda.pizza.ProcessConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Component
@Slf4j
public class ProcessService {

    @Autowired
    private RuntimeService runtimeService;

    private String prepareOrderServiceUrl = "http://localhost:8090";

    @Autowired
    private RestTemplate restTemplate;

    public void sendRequest(DelegateExecution execution)
    {
        log.info("Calling the Prepare-Order-Process for process instance id : {}", execution.getProcessInstanceId());

        //Request.
        StartProcessInstanceDto request  = new StartProcessInstanceDto();
        Map<String, VariableValueDto> variables = new HashMap();
        //order process instance ID (for later correlation)
        VariableValueDto varValue = new VariableValueDto();
        varValue.setValue(execution.getProcessInstanceId());
        variables.put(ProcessConstants.ORDER_PROCESS_INSTANCE_ID, varValue);
        // Order Detail
        varValue = new VariableValueDto();
        varValue.setValue(execution.getVariable(ProcessConstants.CLIENT_ORDER_DETAIL));
        variables.put(ProcessConstants.CLIENT_ORDER_DETAIL, varValue);
        request.setVariables(variables);

        ResponseEntity<ProcessInstanceDto> httpResponse =
                restTemplate.postForEntity(
                        prepareOrderServiceUrl + "/request-order-preparation"
                        , request
                        , ProcessInstanceDto.class);

        if (httpResponse.getStatusCodeValue() != 200) {

        }

        ProcessInstanceDto responseObj = httpResponse.getBody();
        log.info("Prepare-Order-Process started with process instance id {}", responseObj.getId());
        if (responseObj != null) {
            execution.setVariable(ProcessConstants.PREPARE_ORDER_PROCESS_INSTANCE_ID, responseObj.getId());
        }
    }

}
