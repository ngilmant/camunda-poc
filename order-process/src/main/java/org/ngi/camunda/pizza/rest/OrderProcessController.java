package org.ngi.camunda.pizza.rest;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.TaskService;
import org.camunda.bpm.engine.runtime.MessageCorrelationResult;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.task.Task;
import org.ngi.camunda.pizza.orderprocessapispec.OrderPreparedRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class OrderProcessController {

    @Autowired
    private RuntimeService runtimeService;

    @PostMapping("/order-callback")
    public ResponseEntity<Void> orderPrepared(@RequestBody OrderPreparedRequest request) {

        log.info("Order-Process CallBack From Prepare-Order-Process ");

        MessageCorrelationResult messageCorrelationResult = runtimeService
                .createMessageCorrelation("ORDER_PREPARED")
                .processInstanceId(request.getOrderProcessInstanceId())
                .setVariables(null)
                .correlateWithResult();


        return new ResponseEntity<>(HttpStatus.OK);
    }
}
