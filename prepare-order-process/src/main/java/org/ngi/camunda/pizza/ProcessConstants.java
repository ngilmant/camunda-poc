package org.ngi.camunda.pizza;

public class ProcessConstants {

  public static final String PROCESS_DEFINITION_KEY = "prepare-order-process"; // BPMN Process ID


  public static final String ORDER_PROCESS_INSTANCE_ID = "orderProcessInstanceId";
  public static final String CLIENT_ORDER_DETAIL = "orderDetail";
}
