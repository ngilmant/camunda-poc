package org.ngi.camunda.pizza.events;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.ExecutionListener;
import org.camunda.bpm.extension.reactor.bus.CamundaSelector;
import org.camunda.bpm.extension.reactor.spring.listener.ReactorExecutionListener;
import org.springframework.stereotype.Component;

@Component
@CamundaSelector
@Slf4j
public class EventProcessExecutionListener  extends ReactorExecutionListener {

    @Override
    public void notify(DelegateExecution execution) throws Exception {
        log.info("Custom EventProcessListener Called.");
    }
}
