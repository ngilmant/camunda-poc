package org.ngi.camunda.pizza.process;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.ngi.camunda.pizza.ProcessConstants;
import org.ngi.camunda.pizza.orderprocessapispec.OrderPreparedRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
@Slf4j
public class ProcessService {

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private RestTemplate restTemplate;
    private String orderServiceUrl = "http://localhost:8080";

    public void sendOrder(DelegateExecution execution) {

        log.info("send Order-Process Callback for Prepare-Order-Process instance id : {} and targetting Order-Process instance id :{}", execution.getProcessInstanceId(), execution.getVariables().get(ProcessConstants.ORDER_PROCESS_INSTANCE_ID));

        //Request
        OrderPreparedRequest request = new OrderPreparedRequest();
        request.setOrderProcessInstanceId((String)execution.getVariables().get(ProcessConstants.ORDER_PROCESS_INSTANCE_ID));
        request.setMessage("Enjoy your meal");

        ResponseEntity<Void> responseEntity = restTemplate.postForEntity(orderServiceUrl + "/order-callback", request, Void.class);
        log.info("Callback to Order-Process HTTP status {0}", responseEntity.getStatusCodeValue());
    }

 }
