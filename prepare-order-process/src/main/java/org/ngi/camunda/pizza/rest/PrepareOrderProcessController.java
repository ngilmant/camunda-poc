package org.ngi.camunda.pizza.rest;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.rest.dto.VariableValueDto;
import org.camunda.bpm.engine.rest.dto.runtime.ProcessInstanceDto;
import org.camunda.bpm.engine.rest.dto.runtime.StartProcessInstanceDto;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.ngi.camunda.pizza.ProcessConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@Slf4j
public class PrepareOrderProcessController {

    @Autowired
    private RuntimeService runtimeService;

    @PostMapping("/request-order-preparation")
    public ResponseEntity<ProcessInstanceDto> requestOrderPraparation(@RequestBody StartProcessInstanceDto request) {

        log.info("Request order Preparation received from Order-Process instance id {}", request.getVariables().get(ProcessConstants.ORDER_PROCESS_INSTANCE_ID));
        log.info("Starting a new Prepare-Order-Process instance.....");

        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(ProcessConstants.PROCESS_DEFINITION_KEY, parseHttpRequestVariables(request.getVariables()));
        ProcessInstanceDto response = new ProcessInstanceDto(processInstance);

        return new ResponseEntity(response, HttpStatus.OK);
    }

    private Map<String, Object> parseHttpRequestVariables(Map<String, VariableValueDto> requestVariables) {
        Map<String, Object> parsedRequestVariables = new HashMap();

        if (requestVariables != null && !requestVariables.isEmpty()) {
            requestVariables.forEach((k, v) -> {
                parsedRequestVariables.put(k, v.getValue());
            });
        }

        return parsedRequestVariables;
    }
}
